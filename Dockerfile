FROM alpine:3.8
RUN apk update && apk add bash python pip
COPY pipe.sh /
ENTRYPOINT ["/pipe.sh"]
import sys

import pandas as pd
import requests

package_name = sys.argv[1]

url = f"https://anaconda.org/search?q={package_name}"

HEADERS = ({'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36',  # noqa: E501
            'Accept-Language': 'en-US, en;q=0.5',
            "X-Requested-With": "XMLHttpRequest"})

# Request html page with table
r = requests.get(url, headers=HEADERS)

page = pd.read_html(r.text)
table = page[0]

# Search the word in a column 'Package (owner / package)'
rows_with_word = table[
    table["Package (owner / package)"].str.contains(
        rf"\s\s{package_name}\s\s", regex=True, case=False
    )
]
# Filter only conda-forge channel
rows_with_word_conda_forge = rows_with_word[
    rows_with_word["Package (owner / package)"].str.contains("conda-forge")
]

# Find most downloaded package
most_downloads = rows_with_word["Downloads"].max()

try:
    package_table_name = rows_with_word_conda_forge["Package (owner / package)"][0]  # noqa: E501
    package_table_name = package_table_name.replace("conda-forge  /  ", "").replace(  # noqa: E501
        "conda", ""
    )
    print("Popularity: ", most_downloads, "downloads - ", package_table_name)
except Exception as e:
    print("Oops!", e.__class__, "occurred.")

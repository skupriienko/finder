#!/bin/bash

# Receive a first argument from CLI
WORD=$1

# Populate the first array with sorted unique package names (the first level of dependency) from aggregate repo
echo "The package --""$WORD""-- is mentioned inside the packages:"
ARRAY="$(rg -i "\b$WORD\b" aggregate/*/recipe/meta.yaml | sed -e 's!-feedstock/recipe/meta.yaml:.*!!' \
| sed -e 's!aggregate/!!' | sort | uniq)"

counter=1

# For each package name from the first array (1st level of dependency)
for PACKAGE in $ARRAY
do
        # Populate the second array with sorted unique package names (2nd  level of dependency) from aggregate repo
        ARRAY_SECOND="$(rg -i "$PACKAGE" aggregate/*/recipe/meta.yaml | sed -e 's!-feedstock/recipe/meta.yaml:.*!!' \
| sed -e 's!aggregate/!!' | sort | uniq)"
        
        # If $PACKAGE name is the same as WORD name we skip to print it and do not count
        if [[ "$PACKAGE" == "$WORD" ]]; then
                printf ""
        # $PACKAGE name is not empty string
        elif [[ "$PACKAGE" != "" ]]; then
                # Print counter number and the name from the first array (1st level of dependency)
                printf "%d) %s <-- used by: " "$counter" "$PACKAGE"
                
                (( counter++ ))

                # For each package name from the second array (2nd level of dependency)
                for PACKAGE_SECOND in $ARRAY_SECOND
                do         
                # If $PACKAGE name from the second array (2nd level) is not the same as $PACKAGE name from the first array (1st level) we skip to print it and do not count
                if [[ "$PACKAGE_SECOND" != "$PACKAGE" ]]; then
                        # Print only the $PACKAGE_SECOND" name from the second array (2nd level)
                        printf "%s, " "$PACKAGE_SECOND"
                fi             
                done
                echo
        fi
done
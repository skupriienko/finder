import sys
import time

import requests

# Receive an argument from CLI
dev_url = sys.argv[1]

# Common names of branches
master_branch = "/blob/master/"
main_branch = "/blob/main/"

# Common names of license files
license_file = "LICENSE"
copying_file = "COPYING"
# Dash symbol
dash = "-"

header = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36",  # noqa: E501
    "X-Requested-With": "XMLHttpRequest",
}


# Request and check a proper branch name and license file
def github_license_url_exists(base_url):
    master_license = f"{base_url}{master_branch}{license_file}"
    master_copying = f"{base_url}{master_branch}{copying_file}"
    main_license = f"{base_url}{main_branch}{license_file}"
    main_copying = f"{base_url}{main_branch}{copying_file}"
    # If a branch name is 'master' and check license file is 'LICENSE'
    r_master = requests.get(master_license, stream=True, headers=header)
    if r_master.status_code == 200:
        return print("License file: ", master_license)
    # If license file is 'COPYING'
    elif r_master.status_code == 404:
        time.sleep(1)
        r_master_copying = requests.get(
            master_copying, stream=True, headers=header
            )
        if r_master_copying.status_code == 200:
            return print("License file: ", master_copying)
        elif r_master_copying.status_code == 404:
            time.sleep(1)
            # If a branch name is 'main' and check license file is 'COPYING'
            r_main_copying = requests.get(
                main_copying, stream=True, headers=header
                )
            if r_main_copying.status_code == 200:
                return print("License file: ", main_copying)
            # Il license file has other name
            elif r_main_copying.status_code == 404:
                r_main_license = requests.get(
                    main_copying, stream=True, headers=header
                    )
                if r_main_license == 200:
                    return print("License file: ", main_license)
                else:
                    False
    # If URL access is forbiden
    elif r_master.status_code == 403:
        return print("Error 403 Forbidden")
    # If ULR doesn't exist
    else:
        return print("URL doesn't exist")


if __name__ == "__main__":
    # If 'github' substring is inside the URL
    if "github" in dev_url:
        github_license_url_exists(dev_url)
    # In other case 'False'
    else:
        False

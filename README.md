#  Finder
The script for searching a package in all recipes of aggregate repo and creating a checklist for reviewing PR.



## Requirements

Install additional packages with **conda**:

`conda install lxml pandas ripgrep`

and with **pip**:

`pip install shyaml`



## Run the script

Place files `finder.sh`, `downloads_count.py`, and `check_urls_exist.py`  in a parent folder of aggregate repo. Do the same for [pkg_info.csv](https://github.com/ContinuumIO/pbs-scripts/blob/master/pkg_info.csv) from pbs-scripts repo and *crender* output files: `crender/out_lin64/info_p_usedby.yaml`, `crender/out_lin64/info_notes.yaml`.

A tree of your folder should be like this one:

```bash
.

├── aggregate
	└── conda_build_config.yaml
├── automated-build
├── check_urls_exist.py
├── conda-concourse-ci
├── crender
	└── out_lin64
		├── info_p_usedby.yaml
 		└── info_notes.yaml
├── downloads_count.py
├── finder.sh
├── pkg_info.csv
└── used_by.sh
```

Make finder.sh executable: 

```bash
chmod +x finder.sh
```

Run the script from command line: 

```bash
./finder.sh ${package_name}
```

for example:

```bash
./finder.sh faker
```

#!/bin/bash
# The script for searching the package in all recipes in aggregate repo
# Place the script in a parent folder of aggregate repo
# Run the script from command line: ./finder.sh ${package_name}, for example, ./finder.sh pystan
#Don't forget to make finder.sh as executable: chmod +x finder.sh

# Also install packages that we use
# conda install ripgrep pandas lxml

# pip install shyaml

# Version 0.0.15

PACKAGE_NAME=${1:?"Error: parameter missing PACKAGE name"}
DASHES="................................."
greentext="\033[32m"

echo "We are searching - ""$PACKAGE_NAME"
echo "$DASHES"

# Receiving the package release date from ""https://pypi.org/project/ (it's relevant only for python packages)
echo "Receiving the release date from ""https://pypi.org/project/""$PACKAGE_NAME""/#history"
# Use curl to download html page with flag -L if weneed  redirect (maximum redirects is 2)
curl -sS -L --max-redirs 2 "https://pypi.org/project/""$PACKAGE_NAME""/#history" >pypi_tmp_file

# we recieve a new version of package by searching the last created branch
# in aggregate repository
echo "Receiving the last remote branch on aggregate repo..."
cd aggregate/ || exit
git pull
cd "$PACKAGE_NAME"-feedstock || exit
git checkout master
git pull
LAST_BRANCH=$(git branch -r --sort=-committerdate | head -n 1 | sed -e 's/  origin\///')
# Come back in the parent forder of aggregate repo
cd ../../  || exit

echo
echo "$DASHES"

# Check 1st and 2nd levels of package dependency
# # shellcheck source=./.used_by.sh
source used_by.sh "$PACKAGE_NAME"

echo "$DASHES"
# Find all recipes where the package name is mentioned
echo
rg -i --color=always "\b$PACKAGE_NAME\b" aggregate/*/recipe/meta.yaml | uniq

echo "$DASHES"
echo "We found $(rg -i "\b$PACKAGE_NAME\b" aggregate/*/recipe/meta.yaml |
        uniq | sed -e 's!-feedstock/recipe/meta.yaml:.*!!' | sed -e 's!aggregate/!!' | wc -l) lines"

echo
echo "$DASHES"

echo "Checklist of ""$PACKAGE_NAME"":"
echo

echo "See the last merged PRs  https://github.com/ContinuumIO/pbs-review/pulls?q=is%3Apr+is%3Amerged+sort%3Aupdated-desc+""$PACKAGE_NAME"
#echo "The last merged PR is "
echo

# Check build cadence in pkg_info.csv from the current directory
# (if it's empty you can build a package, but below check if it's not a Major version bumb)
BUILD_CADENCE="$(rg "^$PACKAGE_NAME" pkg_info.csv | awk -F"," 'NR==1{print $2}')"
if [[ "" =~ $BUILD_CADENCE ]]; then
        echo
else
        echo "build_cadence: ""$BUILD_CADENCE"
fi
# Check category of package in pkg_info.csv
CATEGORY="$(rg "^$PACKAGE_NAME" pkg_info.csv | awk -F"," 'NR==1{print $3}')"
SUBCATEGORY="$(rg "^$PACKAGE_NAME" pkg_info.csv | awk -F"," 'NR==1{print $4}')"
PKG_TYPE="$(rg "^$PACKAGE_NAME" pkg_info.csv | awk -F"," 'NR==1{print $5}')"
echo "Category: " "$CATEGORY" \
        "| subcategory: " "$SUBCATEGORY" \
        "| pkg_type: " "$PKG_TYPE"

# Print how many downloads has a package from https://anaconda.org/search?q=
python -m downloads_count "$PACKAGE_NAME"

echo "Branch: ""$LAST_BRANCH"
# Check a version of the package that we have in meta.yaml from aggregate repo
VERSION=$(rg -i "set version = " aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | sed -e 's/{% set version = \"//' | sed -e 's/\" %}//')

# Check the last version of the package that we have in the updated (last) branch of the package
UPDATE_VERSION=$(echo "$LAST_BRANCH" | sed -e 's/^pbs_//' | sed 's/_.*//')

# Print version change and major version bump warning
if [[ "${VERSION:0:1}" != "${UPDATE_VERSION:0:1}" ]]; then
        echo "Version change: bump version number from" "$VERSION"" to ""$UPDATE_VERSION" "(Major version bump! Need regression testing!)"
elif [[ "${VERSION:0:2}" != "${UPDATE_VERSION:0:2}" ]]; then
        echo "Version change: bump version number from" "$VERSION"" to ""$UPDATE_VERSION" "(Major version bump! Need regression testing!)"
else
        echo "Version change: bump version number from" "$VERSION"" to ""$UPDATE_VERSION"
fi

# Print a link to the updated branch in Anacondarecipes/aggregate repo
#echo "Update branch: https://github.com/AnacondaRecipes/""$PACKAGE_NAME""-feedstock/blob/""$LAST_BRANCH""/recipe/meta.yaml"

# Print a link of the package in the conda-forge repo
echo "Requirements from conda-forge: https://github.com/conda-forge/""$PACKAGE_NAME""-feedstock/blob/master/recipe/meta.yaml"

# A link of the build on concourse
echo "Concourse build: https://concourse.build.corp.continuum.io/teams/main/pipelines/auto-build-""$PACKAGE_NAME""-""$UPDATE_VERSION"

# Check the license of the package in meta.yaml from aggregate repo
rg -iwN "license:" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml
# Find license file from meta.yaml
LICENSE_FILE="$(rg -i "license_file:" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | sed -e 's/license_file: //' | sed -e 's!-feedstock/recipe/meta.yaml:!!' | sed -e 's!aggregate/!!')"

RELEASE_DATE="$(cat <pypi_tmp_file | grep -i -A2 "class=\"release__version-date\"" | head -3 | tail -1 | sed 'N;s/\n/ /')"

# Print a link of package releases in PyPi
echo "Release date:""$RELEASE_DATE"", https://pypi.org/project/""$PACKAGE_NAME""/#history"

HOME_URL=$(rg -iw "$PACKAGE_NAME" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | rg -o 'home: https?://[^ ]+' | sed -e 's/home: //')
DEV_URL=$(rg -iw "$PACKAGE_NAME" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | rg -o 'dev_url: https?://[^ ]+' | sed -e 's/dev_url: //')
if [[ $DEV_URL == "" ]]; then
        DEV_URL="$HOME_URL"
fi
if [[ -n "$HOME_URL" ]]; then
        echo "Home url: ""$HOME_URL"
else
        echo "No home url"
fi
if [[ -n "$DEV_URL" ]]; then
        echo "dev_url: ""$DEV_URL"
else
        echo "No dev_url"
fi
DASH="%s"

GITHUB_URL_PROJECT="$DEV_URL"
GITLAB_URL_PROJECT="$DEV_URL"
# Find a name of the changelog file from the downloaded html page
curl -sS -L --max-redirs 2 "$GITHUB_URL_PROJECT" >github_tmp_file.html
curl -sS -L --max-redirs 2 "$GITLAB_URL_PROJECT" >gitlab_tmp_file.html
MAIN="$(cat <github_tmp_file.html | rg -io "/blob/main/" | uniq)"
MASTER="$(cat <github_tmp_file.html | rg -io "/blob/master/" | uniq)"
#MAIN_GITLAB="$(cat <gitlab_tmp_file.html | rg -io "/blob/main/" | uniq)"
MASTER_GITLAB="$(cat <gitlab_tmp_file.html | rg -io "/blob/master/" | uniq)"

# Check if it's a github, gitlab or other links
if [[ "github" =~ $(rg -iw "$PACKAGE_NAME" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | rg -o 'dev_url: https?://[^ ]+' | sed -e 's/dev_url: //' | rg -o 'github') ]]; then

        echo "Bug Tracker: new open issues ""$GITHUB_URL_PROJECT""/issues"
        echo "Github releases:  ""$GITHUB_URL_PROJECT""/releases"

        # Placeholder for changes from the changelog file. The names of the file are diffirent (CHANGES, CHANGELOG, CHANGES.rst, etc.)

        CHANGELOG1="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGELOG\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        CHANGELOG2="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGELOG.txt\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        CHANGELOG3="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGELOG.md\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        CHANGELOG4="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGELOG.rst\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        CHANGELOG5="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGES\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        CHANGELOG6="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGES.txt\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        CHANGELOG7="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGES.md\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        CHANGELOG8="$(cat <github_tmp_file.html | grep -i -A2 "title=\"CHANGES.rst\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"

        # Find a name of the license file from the downloaded html page
        # GITHUB_HTML_PAGE="$(curl -sS -L --max-redirs 2 "$GITHUB_URL_PROJECT" > github_tmp_file)"

        # LICENSE1="$(cat < github_tmp_file | grep -i -A2 "title=\"LICENSE\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        # LICENSE2="$(cat < github_tmp_file | grep -i -A2 "title=\"LICENSE.txt\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        # LICENSE3="$(cat < github_tmp_file | grep -i -A2 "title=\"COPYING\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
        # LICENSE4="$(cat < github_tmp_file | grep -i -A2 "title=\"COPYING.txt\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"

        # Print a license file url
        LICENSE_URL="$(python -m check_urls_exist "$GITHUB_URL_PROJECT")"
        echo "$LICENSE_URL"
        if [[ "$MAIN" ]]; then
                if [[ -n "$CHANGELOG1" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG1"
                elif [[ -n "$CHANGELOG2" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG2"
                elif [[ -n "$CHANGELOG3" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG3"
                elif [[ -n "$CHANGELOG4" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG4"
                elif [[ -n "$CHANGELOG5" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG5"
                elif [[ -n "$CHANGELOG6" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG6"
                elif [[ -n "$CHANGELOG7" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG7"
                elif [[ -n "$CHANGELOG8" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MAIN""$CHANGELOG8"
                fi
        else
                if [[ -n "$CHANGELOG1" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG1"
                elif [[ -n "$CHANGELOG2" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG2"
                elif [[ -n "$CHANGELOG3" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG3"
                elif [[ -n "$CHANGELOG4" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG4"
                elif [[ -n "$CHANGELOG5" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG5"
                elif [[ -n "$CHANGELOG6" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG6"
                elif [[ -n "$CHANGELOG7" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG7"
                elif [[ -n "$CHANGELOG8" ]]; then
                        echo "Upstream Changelog: ""$GITHUB_URL_PROJECT""$MASTER""$CHANGELOG8"
                fi

                #echo "Upstream setup.cfg:  ""$GITHUB_URL_PROJECT""/blob/master/setup.cfg"
                #echo "Upstream setup.py:  ""$GITHUB_URL_PROJECT""/blob/master/setup.py"

                #Find setup file if the package type is 'python'
                # if [[ "$PKG_TYPE" == "python" ]]; then
                #         python -m find_setupfile "$GITHUB_URL_PROJECT"
                # fi
        fi
        if [[ "$PKG_TYPE" == "python" ]]; then

                SETUP_CFG="$(cat <github_tmp_file.html | grep -i -A2 "title=\"setup.cfg\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"
                SETUP_PY="$(cat <github_tmp_file.html | grep -i -A2 "title=\"setup.py\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"

                if [[ -n "$SETUP_CFG" && "$MAIN" ]]; then
                        echo "Upstream setup.cfg:  ""$GITHUB_URL_PROJECT""$MAIN""setup.cfg"
                fi
                if [[ -n "$SETUP_PY" && "$MAIN" ]]; then
                        echo "Upstream setup.py:  ""$GITHUB_URL_PROJECT""$MAIN""setup.py"
                fi
                if [[ -n "$SETUP_CFG" && "$MASTER" ]]; then
                        echo "Upstream setup.cfg:  ""$GITHUB_URL_PROJECT""$MASTER""setup.cfg"
                fi
                if [[ -n "$SETUP_PY" && "$MASTER" ]]; then
                        echo "Upstream setup.py:  ""$GITHUB_URL_PROJECT""$MASTER""setup.py"
                fi

                # Find pyproject.toml form the github page
                PYPROJECT_TOML="$(cat <github_tmp_file.html | grep -i -A2 "title=\"pyproject.toml\"" | cut -d'>' -f3 | cut -d'<' -f1 | head -1)"

                if [[ -n "$PYPROJECT_TOML" && "$MAIN" ]]; then
                        echo "Upstream pyproject.toml:  ""$GITHUB_URL_PROJECT""$MAIN""pyproject.toml"
                elif [[ -n "$PYPROJECT_TOML" && "$MASTER" ]]; then
                        echo "Upstream pyproject.toml:  ""$GITHUB_URL_PROJECT""$MASTER""pyproject.toml"
                fi

        fi
elif [[ "gitlab" =~ $(rg -iw "$PACKAGE_NAME" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | rg -o 'dev_url: https?://[^ ]+' | sed -e 's/dev_url: //' | rg -o 'gitlab') ]]; then
        GITLAB_URL_PROJECT="$DEV_URL"
        echo "Bug Tracker: new open issues ""$GITLAB_URL_PROJECT""/""$DASH""/issues"
        echo "Gitlab releases:  ""$GITLAB_URL_PROJECT""/""$DASH""/releases"

        # Print a license file url
        echo "Gitlab license:  ""$GITLAB_URL_PROJECT""/""$DASH""$MASTER_GITLAB""$LICENSE_FILE"
        if [[ "$PKG_TYPE" == "python" ]]; then
                echo "Upstream setup.cfg:  ""$GITLAB_URL_PROJECT""/""$DASH""$MASTER_GITLAB""setup.cfg"
                echo "Upstream setup.py:  ""$GITLAB_URL_PROJECT""/""$DASH""$MASTER_GITLAB""setup.py"
        fi
elif [[ "*" =~ $(rg -iw "$PACKAGE_NAME" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | rg -o 'dev_url: https?://[^ ]+' | sed -e 's/dev_url: //' | rg -o '(*)') ]]; then
        OTHER_URL_PROJECT="$DEV_URL"
        echo "$OTHER_URL_PROJECT"
elif [[ "" =~ $(rg -iw "$PACKAGE_NAME" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml | rg -o 'dev_url: https?://[^ ]+' | sed -e 's/dev_url: //') ]]; then
        echo "Don't find github, gitlab or other urls"
fi

echo
echo "The package ""$PACKAGE_NAME"" is mentioned inside the packages:"
ARRAY="$(rg -i "\b$PACKAGE_NAME\b" aggregate/*/recipe/meta.yaml | sed -e 's!-feedstock/recipe/meta.yaml:.*!!' | sed -e 's!aggregate/!!' | sort | uniq)"

for PACKAGE in $ARRAY; do
        printf "%s | " "$PACKAGE"
done
echo

echo
# Check if the package is mentioned in aggregate/conda_build_config.yaml
PINNING_PACKAGE="$(shyaml keys <aggregate/conda_build_config.yaml | rg -i "$PACKAGE_NAME")"
# Check what a version of the package is pinning
# If it's not mentioned send the error to 2>dev/null
PINNING_VERSION="$(shyaml get-value "$PACKAGE_NAME" <aggregate/conda_build_config.yaml 2>/dev/null)"
if [[ -z "$PINNING_PACKAGE" ]]; then
        echo
else
        echo "Don't build the package! It is mentioned in aggregate/conda_build_config.yaml"
        echo "$PINNING_PACKAGE"": ""$PINNING_VERSION"
fi

echo
# USE CRENDER YAML FILES
echo "Versions used by other packages"
# Find all packages used by the recipe
USED_BY="$(shyaml get-value "$PACKAGE_NAME" < crender/out_lin64/info_p_usedby.yaml)"
if [[ -z "$USED_BY" ]]; then
        echo "No packages that use ""$PACKAGE_NAME"
else
        echo "$USED_BY"
fi
# echo "Current dependencies of the package:"
# # Print a name of package from out_lin64/info_p_dependson.yaml
# rg "^$PACKAGE_NAME " crender/out_lin64/info_p_dependson.yaml | head -n 1
# # Print the package body from out_lin64/info_p_dependson.yaml
# rg -A 50 "^$PACKAGE_NAME\b " crender/out_lin64/info_p_dependson.yaml | sed '2,/:/!d' | awk 'NR>1{print buf}{buf = $0}'

echo
echo -e "$greentext""Recommend to check:"
# Print a name of package from out_lin64/info_notes.yaml
#rg "^$PACKAGE_NAME:" crender/out_lin64/info_notes.yaml | head -n 1
# Print the package body from out_lin64/info_notes.yaml
#rg -A 10 "^$PACKAGE_NAME:" crender/out_lin64/info_notes.yaml | sed '2,/:/!d' | awk 'NR>1{print buf}{buf = $0}'
shyaml get-value "$PACKAGE_NAME" <crender/out_lin64/info_notes.yaml 2>/dev/null

# Check if a package is a GUI application
APP="$(rg -il ".*app:" aggregate/"$PACKAGE_NAME"-feedstock/recipe/meta.yaml  | sed -e 's!-feedstock/recipe/meta.yaml!!' | sed -e 's!aggregate/!!')"
if [[ -z "$APP" ]]; then
        echo
else
        echo
        echo "Recommend to make testing in c3i_test2 channel!"
        echo "$APP"" is a GUI aplication"
fi

echo
echo "Actions:"
echo "1."

echo
echo "Result:"
echo "- all-succeeded"
echo "$DASHES"
